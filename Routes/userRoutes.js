const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const userController = require('../Controllers/userController.js')

//[ Routes without Params ]

//this is responsible for the registration of the user 
router.post('/register', userController.userRegistration);

//this route is for the user authentication
router.post('/login', userController.userAuthentication);



module.exports = router;