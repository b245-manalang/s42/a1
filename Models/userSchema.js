const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No. is required!']
	},
	myOrders: [
			{
				orderId: {
					type: String,
					required: [true, 'orderId is required!']
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'In process'
				}
			}
		]
})


module.exports = mongoose.model('User', userSchema);